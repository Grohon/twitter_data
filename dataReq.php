<?php
ini_set('max_execution_time', 3000);
use voku\db\DB;

require_once 'vendor/autoload.php';

$db = DB::getInstance('localhost', 'root', null , 'twitter_data');
$client = new \GuzzleHttp\Client();

$result = $db->query("SELECT DISTINCT from_user FROM bot_data");
$distinct_bots  = $result->fetchAllArray();

//find already analysed tweets to skip them
$tresult = $db->query("SELECT id_str FROM sentiment_analysis");
$tweetsAnalyzed  = $tresult->fetchAllArray();
$already = array();

foreach ($tweetsAnalyzed as $key => $value) {
  $already[] = $value["id_str"];
}

function getDataAndAnalyze($handle){
  global $db, $client, $already;
  // 50 tweets from each user/handle
  $result = $db->query("SELECT id_str, text FROM bot_data where from_user = '$handle' 
    AND id_str NOT IN ( '" . implode( "', '" , $already ) . "' )
    Order by id_str ASC LIMIT 50");

  $tweet_text  = $result->fetchAllArray();

  foreach ($tweet_text as $tweet) {
    // Sentiment analysis parameters
    $senti_params = array(
      'form_params' => array(
        'text' => $tweet['text']
      )
    );
    
    //continue if already in DB
    //disabled very slow
    // if(in_array($tweet['id_str'] , $already))
    //   continue; 

    // Make API Call and get response
    $res = $client->request('POST', 'http://text-processing.com/api/sentiment/', $senti_params);
    // $res = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');

    if( $res->getStatusCode() == 200 ){
      // echo $res->getBody();
      $sentiment = json_decode($res->getBody(), TRUE);
      $array_insert = [];
      foreach ($sentiment as $value) {
        
        $array_insert = array(
          'id_str' => $tweet['id_str'],
          'negative' => $sentiment['probability']['neg'],
          'neutral' => $sentiment['probability']['neutral'],
          'positive' => $sentiment['probability']['pos'],
          'label' => $sentiment['label'],
        );
      }
      $db->insert( 'sentiment_analysis', $array_insert );
    }
    else{
      echo "<h1>Error {$res->getStatusCode()}</h1>";
    }
  }
}

foreach ($distinct_bots as $key => $value) {
  getDataAndAnalyze($value['from_user']);
}
// getDataAndAnalyze();

?>
